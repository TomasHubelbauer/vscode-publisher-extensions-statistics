# VS Code Publisher Extensions Statistics

This NodeJS application continuously scans the VS Code Marketplace API and fetches statistics of all published extensions of a given publisher.

It serves a page which shows the current values of the statistics and plots the historical data to a graph.

## Running

### Locally

- `yarn start`
- `start http://localhost:6380`

### Remotely

This application is deployed on [Now](https://now.sh).

[Demo](https://th.now.sh/)

Now runs [`yarn now-start`](https://zeit.co/now#how-does-my-app-detect-now).

## Sleeping

This is supposed to be a watchdog script and as such it can't go to sleep.

https://zeit.co/docs/getting-started/scaling

- [ ] Set this up to avoid the *Too many instances* error when deploying

## Deploying

- Bump version
- Update changelog
- `yarn deploy` (`now && now alias`)
  - [ ] Consider extending this to remove previous instances

## Contributing

Feel free!
