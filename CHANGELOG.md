# VS Code Publisher Extensions Statistics

## `2.0.0` (2018-09-22)

- Fix a problem where extensions for another publisher were fetched with mine
- Make extension names clickable

## `1.0.0` (2018-04-28)

Initial version capturing the statistics from the Marketplace API and storing them in Postgres.
