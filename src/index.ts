import fetch from 'node-fetch';
import * as express from 'express';
import { ensureDir, copy, appendFile, writeJson } from 'fs-extra';
import { join } from 'path';
import { tmpdir } from 'os';

const tempDirectoryPath = join(tmpdir(), 'vscode-publisher-extension-statistics');

const url = 'https://marketplace.visualstudio.com/_apis/public/gallery/extensionquery?api-version=2.0-preview';
const body = JSON.stringify({
  filters: [
    {
      criteria: [
        {
          filterType: 8,
          value: "Microsoft.VisualStudio.Code"
        },
        {
          filterType: 10,
          value: "publisher:\"Tomas Hubelbauer\""
        }
      ]
    }
  ],
  flags: 914
});

const server = express();

server.enable('trust proxy');

console.log('Serving…');
server.use(express.static(tempDirectoryPath));

server.listen(6380 /* https://zeit.co/docs/deployment-types/node#port-selection */, async () => {
  console.log('Listening…');

  console.log('Preparing…', tempDirectoryPath);
  await ensureDir(tempDirectoryPath);
  await copy('src/index.js', join(tempDirectoryPath, 'index.js'));
  await copy('src/index.html', join(tempDirectoryPath, 'index.html'));

  console.log('Waiting…');
  let firstWait = true;
  // Update the values periodically, very often locally in development, less often in deployment
  while (firstWait || await wait(60 * (process.env.NOW ? 15 : 1))) {
    try {
      console.log('Fetching…');
      const response = await fetch(url, { method: 'POST', headers: { 'Content-Type': 'application/json' }, body });
      const data = await response.json();

      console.log('Filtering…');
      const extensions = data.results[0].extensions;
      const publishedExtensions = extensions.filter(extension => extension.flags.indexOf('unpublished') === -1);

      console.log('Storing…');
      for (const extension of publishedExtensions) {
        let record = `${extension.displayName};${Date.now()};`;
        for (const statisticName of ['install', 'averagerating', 'ratingcount', 'trendingdaily', 'trendingmonthly', 'trendingweekly', 'updateCount']) {
          const statistic = extension.statistics.find(statistic => statistic.statisticName === statisticName);
          if (statistic !== undefined) {
            record += statistic.value;
          }

          record += ';';
        }

        await appendFile(join(tempDirectoryPath, extension.extensionName + '.csv'), record + `\n`);
      }

      console.log('Indexing…');
      await writeJson(
        join(tempDirectoryPath, 'index.json'),
        {
          extensions: publishedExtensions
            .map(({ displayName, extensionName, statistics }) => {
              const install = statistics.find(({ statisticName }) => statisticName === 'install');
              const updateCount = statistics.find(({ statisticName }) => statisticName === 'updateCount');
              return { displayName, extensionName, install: install && install.value || 0, updateCount: updateCount && updateCount.value || 0 };
            })
            .sort((a, b) => (b.install + b.updateCount) - (a.install + a.updateCount)),
          timestamp: Date.now(),
        },
        { spaces: 2 }
      );
    } catch (error) {
      console.log('Erroring…');
      console.log(error);
    }

    firstWait = false;
  }
});

function wait(timeoutSeconds) {
  return new Promise(resolve => {
    setTimeout(resolve, timeoutSeconds * 1000, true);
  });
}
