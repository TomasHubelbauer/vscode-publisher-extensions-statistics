window.addEventListener('load', async _ => {
  try {
    const indexResponse = await fetch('/index.json');
    const indexData = await indexResponse.json();
    for (const { displayName, extensionName, install, updateCount } of indexData.extensions) {
      const extensionDiv = document.createElement('div');
      const extensionA = document.createElement('a');
      extensionA.textContent = displayName;
      extensionA.href = `https://marketplace.visualstudio.com/items?itemName=TomasHubelbauer.${extensionName}`;
      extensionA.target = '_blank';
      const statisticsSpan = document.createElement('span');
      statisticsSpan.textContent = ` has ${install} installs and ${updateCount} updates (total: ${install + updateCount})`;
      extensionDiv.appendChild(extensionA);
      extensionDiv.appendChild(statisticsSpan);
      document.body.appendChild(extensionDiv);
    }

    const timestampDiv = document.createElement('div');
    timestampDiv.textContent = `Accurate as of ${new Date(indexData.timestamp).toLocaleString()}`;
    document.body.appendChild(timestampDiv);
  } catch (error) {
    console.log(error);
    const errorText = document.createTextNode('Failed to load');
    document.body.appendChild(errorText);
  }
});
